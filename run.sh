#!/bin/bash

if [[ $SERVER_NAME ]]; then
  SERVER_NAME_CONFIG="server_name ${SERVER_NAME};"
fi

if [[ $ACCESS_KEY ]] && [[ $SECRET_KEY ]] && [[ $S3_BASE_URL ]] && [[ $REGION ]] && [[ $BUCKET_NAME ]]; then
  SIGNING=$(/generate_signing_key -k ${SECRET_KEY} -r ${REGION})
  SIGNING_KEY=$(echo $SIGNING | awk '{print $1}')
  KEY_SCOPE=$(echo $SIGNING | awk '{print $2}')
  KEY_CONFIG="aws_access_key ${ACCESS_KEY};
    aws_key_scope ${KEY_SCOPE};
    aws_signing_key ${SIGNING_KEY};
    aws_s3_bucket ${BUCKET_NAME};"
  PROXY_CONFIG="aws_sign;
    proxy_pass ${S3_BASE_URL}/${BUCKET_NAME};
    proxy_set_header Host '${S3_BASE_URL}';
    proxy_set_header Authorization '';
    proxy_hide_header x-amz-id-2;
    proxy_hide_header x-amz-request-id;
    proxy_hide_header x-amz-meta-server-side-encryption;
    proxy_hide_header x-amz-server-side-encryption;
    proxy_hide_header Set-Cookie;
    proxy_ignore_headers "Set-Cookie";
    proxy_intercept_errors on;"
fi

if [[ $CACHE_PATH ]]; then
  chown -R nginx:root ${CACHE_PATH}
  CACHE_PATH_CONFIG="proxy_cache_path ${CACHE_PATH} levels=1:2 keys_zone=${CACHE_NAME}:1024m max_size=${CACHE_SIZE} inactive=${CACHE_INACTIVE} use_temp_path=off;"
  CACHE_CONFIG="proxy_cache ${CACHE_NAME};
    proxy_cache_revalidate on;
    proxy_cache_valid 200 302 404 5m;
    proxy_cache_use_stale error timeout updating http_500 http_502 http_503 http_504;
    proxy_cache_background_update on;
    proxy_cache_lock on;
    proxy_cache_lock_timeout 5m;"
fi

if [[ $SERVER_NAME ]]; then
/bin/cat <<EOF > /etc/nginx/conf.d/${SERVER_NAME}.conf

${CACHE_PATH_CONFIG}

log_format ecs_access_logger escape=json '{'
  '"@timestamp":"$time_iso8601",'
  '"service.name":"${SERVER_NAME}",'

  '"url.path":"$request_uri",'
  '"http.request.method":"$request_method",'
  '"http.request.duration":"$request_time",'
  '"http.request.referrer":"$http_referer",'
  '"user_agent.original":"$http_user_agent",'
  '"client.nat.ip":"$remote_addr",'
  '"client.ip":"$http_x_forwarded_for",'
  '"user.name":"$remote_user",'

  '"http.response.body.bytes":"$body_bytes_sent",'
  '"http.response.bytes":"$bytes_sent",'
  '"http.response.status_code":"$status"'
'}';

server {
  listen 80;
  ${SERVER_NAME_CONFIG}
  ${KEY_CONFIG}

  location / {
    root       /var/www/html;
    autoindex  on;
    access_log /var/log/nginx/access.log ecs_access_logger;
    error_log  /var/log/nginx/error.log warn;

    ${PROXY_CONFIG}
    ${CACHE_CONFIG}
  }

  location /healthcheck {
    access_log off;
    stub_status on;
  }
}
EOF
fi

/usr/sbin/nginx
