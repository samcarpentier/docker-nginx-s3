# Nginx with AWS Authentication Plugin

This Docker image runs Ubuntu with Nginx compiled with ngx_aws_auth module.
It uses Amazon's V4 API for authentication against an S3 bucket.

Currently only "path style" access is implemented. This is useful for on-premises S3 deployments.

## Settings

Several options are customizable using environment variables.

| Variable | Description |
| :--- | :--- |
| `S3_BASE_URL` | The base URL of the S3 API (i.e.: https://s3.amazonaws.com) |
|`BUCKET_NAME`| The name of you S3 bucket |
|`ACCESS_KEY`| The Access Key that gives access to your S3 bucket |
|`SECRET_KEY`| The Secret Key that gives access to your S3 bucket |
|`REGION`| The Region that your bucket is hosted at |
|`SERVER_NAME`| Set a server name if you want to have a unique name set in your Nginx config |
|`CACHE_PATH`| Set a path for a cache folder if you wan't to enable the caching for Nginx |
|`CACHE_NAME`| If you want a unique name for your cache (in case of multiple). Defaults to  `edge-cache` |
|`CACHE_SIZE`| Set a max size for you cache. Defaults to ``1g`` |
|`CACHE_INACTIVE`| Set how long to keep things in cache. Defaults to ``1d`` |
